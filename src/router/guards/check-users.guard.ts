import store from "@/store";
import {getUsers} from "@/store/actions";

export default function checkUsers(): boolean {
  store.dispatch(getUsers).then();
  return store.state.users.length >= 2;
}
