import Vue from "vue";
import VueRouter, {Route, RouteConfig} from "vue-router";
import store from "@/store";
import {getMeetings, getUsers, initHomePage, initUsersPage} from "@/store/actions";
import {MessageBox} from "element-ui";
import checkUsers from "@/router/guards/check-users.guard";

Vue.use(VueRouter);

const routes: RouteConfig[] = [
  {
    path: "/",
    name: "Meetings",
    component: () => import( "@/views/home/home.vue"),
    beforeEnter: (to: Route, from: Route, next: () => void) => {
      store.dispatch(initHomePage).then(() => next());
    },
  },
  {
    path: "/users",
    name: "Users",
    component: () => import("@/views/users/users.vue"),
    beforeEnter: (to: Route, from: Route, next: () => void) => {
      store.dispatch(initUsersPage).then(() => next());
    },
  },
  {
    path: "/add-meeting",
    name: "New meeting",
    component: () => import("@/views/add-meeting/add-meeting.vue"),
    beforeEnter: (to: Route, from: Route, next: () => void) => {
      if (checkUsers()) {
        Promise.all([store.dispatch(getMeetings), store.dispatch(getUsers)]).then(() => {
          next();
        });
      } else {
        MessageBox.confirm("Для создания встречи необходиом как минимум два пользователя. Хотите добавить нового пользователя?", {
          cancelButtonText: "Отмена",
          confirmButtonText: "Добавить",
        }).then(() => {
          if (from.path !== "/add-user") { router.push("/add-user").then(); }
        }).catch();
      }
    },
  },
  {
    path: "/add-user",
    name: "New user",
    component: () => import("@/views/add-user/add-user.vue"),
  },
  {
    path: "*",
    name: "404",
    component: () => import("@/views/404.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
