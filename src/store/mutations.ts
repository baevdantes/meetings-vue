import {MutationTree} from "vuex";
import IAppState from "@/interfaces/IAppState";
import IMeeting from "@/interfaces/IMeeting";
import IUser from "@/interfaces/IUser";

export const setMeetings = "setMeetings";
export const setUsers = "setUsers";
export const setMeeting = "setMeeting";

const mutations: MutationTree<IAppState> = {
  [setMeetings]: (state: IAppState, meetings: IMeeting[]) => {
    state.meetings = meetings;
  },

  [setUsers]: (state: IAppState, users: IUser[]) => {
    state.users = users;
  },

  [setMeeting]: (state: IAppState, meeting: IMeeting) => {
    state.currentMeeting = meeting;
  },
};

export default mutations;
