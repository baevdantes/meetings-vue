import IAppState from "@/interfaces/IAppState";

const state: IAppState = {
  meetings: [],
  users: [],
  currentMeeting: null,
};

export default state;
