import {ActionTree} from "vuex";
import IAppState from "@/interfaces/IAppState";
import router from "@/router";
import {IMeetingForm} from "@/interfaces/IMeeting";
import {setMeetings, setUsers} from "@/store/mutations";
import api from "@/api";
import {IUserForm} from "@/interfaces/IUser";

export const getMeetings = "getMeetings";
export const getUsers = "getUsers";
export const addMeeting = "addMeeting";
export const addUser = "addUser";
export const initHomePage = "initHomePage";
export const initUsersPage = "initUsersPage";

const actions: ActionTree<IAppState, IAppState> = {
  [getMeetings]: ({commit}) => {
    return api.getMeetings() ? commit(setMeetings, api.getMeetings()) : commit(setMeetings, []);
  },

  [getUsers]: ({commit}) => {
    return api.getUsers() ? commit(setUsers, api.getUsers()) : commit(setUsers, []);
  },

  [addMeeting]: ({commit, dispatch, state}, meeting: IMeetingForm) => {
    api.addMeeting(meeting);
    commit(setMeetings, dispatch(getMeetings));
    router.push("/").then();
  },

  [addUser]: ({commit, dispatch}, user: IUserForm) => {
    api.addUser(user);
    commit(setUsers, dispatch(getUsers));
    router.push("/users").then();
  },

  [initHomePage]: ({dispatch}) => {
    dispatch(getMeetings).then();
  },

  [initUsersPage]: ({dispatch}) => {
    dispatch(getUsers).then();
  },
};

export default actions;
