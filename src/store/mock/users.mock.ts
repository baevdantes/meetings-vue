import IUser from "@/interfaces/IUser";

const mockUsers: IUser[] = [
  {
    id: 1,
    name: "John",
    surname: "Doe",
  },
  {
    id: 2,
    name: "Bill",
    surname: "Grey",
  },
];

export default mockUsers;
