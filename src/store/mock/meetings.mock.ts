import IMeeting from "@/interfaces/IMeeting";

const mockMeetings: IMeeting[] = [
  {
    id: 1,
    title: "Meeting 1",
    period: {
      start: "09:00",
      end: "15:00",
    },
    date: new Date(),
    role1: null,
    role2: null,
    users: [],
  },
];

export default mockMeetings;
