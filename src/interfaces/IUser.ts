export interface IUserForm {
  name: string;
  surname: string;
}

export default interface IUser extends IUserForm {
  id: number;
}
