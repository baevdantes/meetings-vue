export default interface IPeriod {
  start: string;
  end: string;
}
