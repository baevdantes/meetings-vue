import IUser from "@/interfaces/IUser";
import IPeriod from "@/interfaces/IPeriod";

export interface IMeetingForm {
  title: string;
  users: IUser[];
  date: Date;
  period: IPeriod;
  role1: IUser;
  role2: IUser;
}

export default interface IMeeting extends IMeetingForm {
  id: number;
}

export enum EnumStatus {
  planned,
  current,
  finished
}

export enum EnumStatusTitle {
  "Запланировна" = EnumStatus.planned,
  "Проходит сегодня" = EnumStatus.current,
  "Завершена" = EnumStatus.finished,
}

