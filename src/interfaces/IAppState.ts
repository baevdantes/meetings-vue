import IUser from "@/interfaces/IUser";
import IMeeting from "@/interfaces/IMeeting";

export default interface IAppState {
  meetings: IMeeting[];
  currentMeeting: IMeeting;
  users: IUser[];
}
