import IUser, {IUserForm} from "@/interfaces/IUser";
import IMeeting, {IMeetingForm} from "@/interfaces/IMeeting";

export default class Api {
  static getUsers() {
    return JSON.parse(localStorage.getItem("users")) ? JSON.parse(localStorage.getItem("users")) : null;
  }

  static getMeetings() {
    return JSON.parse(localStorage.getItem("meetings")) ? JSON.parse(localStorage.getItem("meetings")) : null;
  }

  static addUser(user: IUserForm) {
    const dataUsers: IUser[] = this.getUsers();
    if (!dataUsers) {
      localStorage.setItem("users", JSON.stringify([
        {...user, id: 1},
      ]));
    } else {
      dataUsers.unshift({...user, id: dataUsers.length + 1});
      localStorage.setItem("users", JSON.stringify(dataUsers));
    }
  }

  static addMeeting(meeting: IMeetingForm) {
    const dataMeetings: IMeeting[] = this.getMeetings();
    if (!dataMeetings) {
      localStorage.setItem("meetings", JSON.stringify([
        {...meeting, id: 1},
      ]));
    } else {
      dataMeetings.unshift({...meeting, id: dataMeetings.length + 1});
      localStorage.setItem("meetings", JSON.stringify(dataMeetings));
    }
  }
}
