import HeaderComponent from "@/components/header.vue";
import {Component, Vue} from "vue-property-decorator";
import store from "@/store";

window.addEventListener("storage", () => {
  store.dispatch("getUsers").then();
  store.dispatch("getMeetings").then();
});

@Component({
  components: {
    HeaderComponent,
  },
})
export default class App extends Vue {
}
