import Vue from "vue";
import app from "./app.vue";
import router from "./router";
import store from "./store";
import "./registerServiceWorker";
import "bootstrap-4-grid/css/grid.min.css";
import "./main.scss";
import "element-ui/lib/theme-chalk/index.css";
import locale from "element-ui/lib/locale/lang/en";
import ElementUI from "element-ui";

Vue.use(ElementUI, {locale});
Vue.config.productionTip = false;
Vue.filter("date", (date: Date) => new Date(date).toLocaleDateString());


new Vue({
  router,
  store,
  render: (h) => h(app),
}).$mount("#app");
