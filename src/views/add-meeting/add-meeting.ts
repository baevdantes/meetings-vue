import {Component, Vue} from "vue-property-decorator";
import btn from "@/components/btn.vue";
import {Action, State} from "vuex-class";
import {isNotEmpty} from "@/utils";
import IMeeting, {IMeetingForm} from "@/interfaces/IMeeting";
import {addMeeting} from "@/store/actions";
import IUser from "@/interfaces/IUser";
import * as _ from "lodash";

const timesRange: { [key: string]: string } = {
  start: "09:00",
  step: "01:00",
  end: "18:00",
};

@Component({
  components: {
    btn,
  },
})
export default class AddMeeting extends Vue {
  @Action(addMeeting) addMeeting: (value: IMeetingForm) => void;
  @State("meetings") meetings: IMeeting[];
  @State("users") users: IUser[];

  form: IMeetingForm = {
    title: "",
    date: null,
    period: {
      start: null,
      end: null,
    },
    users: [],
    role1: null,
    role2: null,
  };
  datePickerOptions: any = {
    disabledDate(date: Date) {
      return date < new Date();
    },
  };

  step: number = 0;

  get availableUsers(): IUser[] {
    const filteredMeetings: IMeeting[] = this.meetings.filter((meeting: IMeeting) => {
      return Number(new Date(meeting.date)) === Number(this.form.date);
    });
    let filteredMeetingsPeriods: IMeeting[] = [];
    if (filteredMeetings.length && this.form.period.start && this.form.period.end) {
      filteredMeetingsPeriods = filteredMeetings.filter((meeting: IMeeting) => {
        const periodMeeting: number[] = this.getTimePeriod(meeting.period.start, meeting.period.end);
        return _.intersection(this.timePeriod, periodMeeting).length;
      });
    } else {
      return this.users;
    }
    let usersOfMeetings: IUser[] = [];
    filteredMeetingsPeriods.forEach((meeting: IMeeting) => {
      usersOfMeetings.push(...meeting.users);
    });
    usersOfMeetings = _.uniqBy(usersOfMeetings, (meeting: IMeeting) => {
      return meeting.id;
    });
    if (filteredMeetingsPeriods.length) {
      return _.differenceWith(this.users, usersOfMeetings, _.isEqual);
    } else {
      return this.users;
    }
  }

  get optionsRole2(): IUser[] {
    return this.availableUsers.filter((user: IUser) => {
      return user !== this.form.role1;
    });
  }

  get optionsOtherUsers(): IUser[] {
    return this.optionsRole2.filter((user: IUser) => {
      return user !== this.form.role2;
    });
  }


  get timePeriod(): number[] {
    return this.getTimePeriod(this.form.period.start, this.form.period.end);
  }

  get isAvailableSelectors(): boolean {
    return this.form.date && this.form.period.end
      && this.form.period.end && this.form.title && isNotEmpty(this.form.title);
  }

  get timePickerOptionsEnd(): any {
    return {
      ...timesRange,
      minTime: this.form.period.start,
    };
  }

  get timePickerOptionsStart(): any {
    return {
      ...timesRange,
      maxTime: this.form.period.end,
    };
  }

  get isValidForm(): boolean {
    return !!(
      this.form.title.length &&
      isNotEmpty(this.form.title) &&
      this.form.date && this.form.period.end && this.form.period.end
      && this.form.role1 && this.form.role2
    );
  }

  getTimePeriod(start: string, end: string) {
    const numbers: number[] = [
      Number(start.split(":")[0]),
      1,
      Number(end.split(":")[0]),
    ];
    const period: number[] = [];
    let current: number = numbers[0];
    while (current <= numbers[2]) {
      period.push(current);
      current = current + numbers[1];
    }
    return period;
  }

  next() {
    this.step++;
  }

  clearDates() {
    this.form.date = null;
    this.form.period.end = null;
    this.form.period.start = null;
  }

  nextAfterStart() {
    if (this.availableUsers.length >= 2) {
      this.next();
    } else {
      this.$alert("На заданные дату и период нет свободных участников. Измените параметры встречи.", {
        confirmButtonText: "ОК",
      }).then(() => {
        this.clearDates();
      }).catch(() => {

      });
    }
  }

  prev() {
    this.step--;
  }

  prevToStart() {
    this.form.role1 = null;
    this.prev();
  }

  prevToRole1() {
    this.form.role2 = null;
    this.prev();
  }

  prevToRole2() {
    this.form.users = [];
    this.prev();
  }

  changeStartTime() {
    if (this.form.period.start >= this.form.period.end) {
      this.form.period.end = null;
    }
  }

  addMeetingBtn() {
    if (this.isValidForm) {
      const formData: IMeetingForm = this.form;
      formData.users.push(this.form.role1, this.form.role2);
      this.addMeeting(formData);
    }
  }
}
