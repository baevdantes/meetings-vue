import {Component, Vue} from "vue-property-decorator";
import card from "@/components/card.vue";
import btn from "@/components/btn.vue";
import {State} from "vuex-class";
import IMeeting, {EnumStatus, EnumStatusTitle} from "@/interfaces/IMeeting";

@Component({
  components: {
    card,
    btn,
  },
})
export default class Home extends Vue {
  @State("meetings") meetings: IMeeting[];

  getMeetingStatus(meeting: IMeeting): string {
    const today: Date = new Date(new Date().setHours(0, 0, 0 , 0));
    const meetingDate: Date = new Date(new Date(meeting.date).setHours(0));
    if (Number(today) < Number(meetingDate)) {
      return EnumStatusTitle[EnumStatus.planned];
    }
    if (Number(today) > Number(meetingDate)) {
      return EnumStatusTitle[EnumStatus.finished];
    }

    if (Number(today) === Number(meetingDate)) {
      return EnumStatusTitle[EnumStatus.current];
    }
  }
}
