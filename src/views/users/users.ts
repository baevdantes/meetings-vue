import {Component, Vue} from "vue-property-decorator";
import {State} from "vuex-class";
import IUser from "@/interfaces/IUser";
import card from "@/components/card.vue";
import btn from "@/components/btn.vue";

@Component({
  components: {
    card,
    btn,
  },
})
export default class Users extends Vue {
  @State("users") users: IUser[];
}
