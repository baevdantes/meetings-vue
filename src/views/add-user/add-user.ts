import {Component, Vue} from "vue-property-decorator";
import {Form, Input} from "element-ui";
import {isNotEmpty} from "@/utils";
import {Action} from "vuex-class";
import {IUserForm} from "@/interfaces/IUser";

@Component({
  components: {
    Input,
  },
})
export default class AddUser extends Vue {
  @Action("addUser") addUser: (formData: IUserForm) => void;

  formData: IUserForm = {
    name: "",
    surname: "",
  };

  get isValidForm(): boolean {
    return !!(
      this.formData.name.length &&
      this.formData.surname.length &&
      isNotEmpty(this.formData.name) &&
      isNotEmpty(this.formData.surname)
    );
  }

  addUserBtn() {
    if (this.isValidForm) {
      this.addUser(this.formData);
    }
  }
}
